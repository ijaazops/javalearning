This is the initial readme file

***************************************************************************************************


Create a Maven Project
Then add the Selenium , Apache POI API and TestNG dependencies to the pom.xml file
Create seperate packages for Pages, Base,Config,TestData,Util in the src/main/java parent package
Then create a seperate package for Testcases in the src/mainn/test parent package
Create a "config.properties" in the config , where u can store the global variables
Create a class for each pages you are goin to automate in Pages package
Create a TestBase class for initializing the constructor and the initialization class
Create a TestUtil class to store common variables that are accessible to all the classes
All the classes should extend the Testbase class
For each page class create a testclass to do all the testing functions
@before method @testMethod and @aftermethod are defined in the test classes
Create a source folder in project src/main/resources and create file name testng.xml and click finish
then paste the default code in the xml file and call the classes in the xml file
You can create different testcase for different xml file. eg-smoke suite, regression suite etc
then do the data driven approach
Create a method for the data driven approach 
First create a excel sheet in TestData package with the relavant fields in the excel file
Create a gettestdata method in the TestUtil java which accepts the testsheet name
Which will return a 2D object array 
Create the method with getdata wih @dataprovider annotation in the tesstpage
Then passs the data provider to the @test to the specific method
Adding @CacheLookup will store the particular Findby element in the cache and the performance of the script will increase
if the element propeties is change then the cache will be corrupt and it will give staleelement exception
Then adding Webdriver fire event will give logs to the program
Copy and paste the webdriver event listener java file to the util package
add the event firing webdriver to the testbase class
We can customize the webdiver fire event to take screenshot when an error is thrown
Create the takescreenshot class in the Testutil class
To add extent report to the program
first add the extent report dependecny to the pom.xml 
then we have to write a extent report listener to listen each and every test case
create a package with Extentreportlistener--> use the template given by extent report listener
Then add the listener with the clas in the testng.xml file
  






************************************************************************************************************


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).